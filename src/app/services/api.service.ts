import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(public http: HttpClient) {
    }

    geturl(url) {
        return this.http.get(url);
    }

    get(url) {
        return this.http.get(url);
    }

    post(url, body: any, reqOpts?: any) {
        return this.http.post(url, body, reqOpts);
    }
}