import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from './utility.service';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
})
export class NetworkService {

    constructor(
        public http: HttpClient,
        public utility: UtilityService,
        public api: ApiService) { }
    // Generic Methods for Http Response
    /**
     * @param key
     * @param data
     * @param id
     * @param showloader
     * @param showError
     */

    sendPushNotification(data, reqOptions) {
        return this.httpPostResponse('https://fcm.googleapis.com/fcm/send', data, null, true, true, 'application/json', reqOptions);
    }


    httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json', reqOptions = {}) {
        return this.httpResponse('post', key, data, id, showloader, showError, contenttype, reqOptions);
    }

    httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json', reqOptions = {}) {
        return this.httpResponse('get', key, {}, id, showloader, showError, contenttype, reqOptions);
    }

    // default 'Content-Type': 'application/json',
    httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json', reqOptions = {} ) {

        return new Promise(resolve => {

            if (showloader == true) {
                this.utility.showLoader();
            }

            const _id = (id) ? '/' + id : '';
            const url = key + _id;
            
            const seq = (type == 'get') ? this.api.get(url) : this.api.post(url, data , reqOptions);

            seq.subscribe((res: any) => {
                if (showloader == true) {
                    this.utility.hideLoader();
                }

                // if (res['status'] != 200) {
                //     if (showError) {
                //         this.utility.presentAlert(res['message']);
                //     }
                //     resolve(null);
                // } else {
                //     resolve(res);
                // }
                resolve(true)

            }, err => {
                if (showloader == true) {
                    this.utility.hideLoader();
                }

                console.log(err);

                resolve(null);

            });

        });

    }


}