import { Injectable } from '@angular/core';
import { FirebaseService } from './firebase.service';
import { UserInterface } from '../models/user';
import { VendorInterface } from '../models/vendor';
import { ItemInterface } from '../models/item';
import { OrderInterface } from '../models/order';
import { CategoryInterface } from '../models/category';
import { SliderInterface } from '../models/slider';
import { CityInterface } from '../models/city';
import { DashboardService } from './dashboard.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  users: any;
  vendors: any;
  orders: any;
  districts: any;
  zones: any;
  sliders: any;
  products: any;
  categories: any;


  vendor: any;
  ven_key: any;

  constructor(
    public firebaseService: FirebaseService,
    private dashboard: DashboardService,
    private auth: AuthService
  ) { 
    // this.initialize();
   }
  getUsers() {
    return new Promise(resolve => {
      this.firebaseService.getUserList().snapshotChanges().subscribe(users => {
        this.users = [];

        users.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.users.push(a as UserInterface);
        });
        resolve(this.users);
      });
    });
  }

  getVendors() {
    return new Promise(resolve => {
      this.firebaseService.getvendor().snapshotChanges().subscribe(vendors => {
        this.vendors = [];
        vendors.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.vendors.push(a as VendorInterface);
        });        
        resolve(this.vendors);
      });
    });

  }

  getProducts() {
    return new Promise(async resolve => {
      console.log(this.products);

        this.vendor = await this.auth.checkVendor();
        console.log(this.vendor);
        if (this.vendor === false) {
          this.firebaseService
            .getItems()
            .snapshotChanges()
            .subscribe(products => {
              this.products = [];
              products.forEach(item => {
  
                let a = item.payload.toJSON();
                a['$key'] = item.key;
  
                this.products.push(a as ItemInterface);
              });
              resolve(this.products);
            });
        } else if (this.vendor === true) {
          this.ven_key = await this.auth.getVendorKey();
          this.firebaseService
            .getVendorItems(this.ven_key)
            .snapshotChanges()
            .subscribe((products) => {
  
              this.products = [];
              products.forEach((item) => {
  
                const a = item.payload.toJSON();
                a['$key'] = item.key;
  
  
                this.products.push(a as ItemInterface);
              });
              this.dashboard.vendorProductCount = this.products.lenght;
              resolve(this.products);
            });
        }
    });
  }

  getCurrentUserOrders() {
    return new Promise(async resolve => {
      console.log(this.orders);
      this.vendor = await this.auth.checkVendor();
      console.log(this.vendor);
      if (this.vendor === false) {

        this.firebaseService
          .getOrders()
          .snapshotChanges()
          .subscribe((orders) => {
            // Using snapshotChanges() method to retrieve list of data along with metadata($key)
            this.orders = [];
            orders.forEach((item) => {
              const a = item.payload.toJSON();
              a['$key'] = item.key;
              this.orders.push(a as OrderInterface);
            });
            resolve(this.orders);
          });

      } else if (this.vendor === true) {
        this.ven_key = await this.auth.getVendorKey();
        this.firebaseService
          .getOrders()
          .snapshotChanges()
          .subscribe((orders) => {
            this.orders = [];
            orders.forEach((item) => {
              const a = item.payload.toJSON();
              a['$key'] = item.key;
              if (a['vendors']) {
              Object.keys(a['vendors']).forEach((key) => {
                let vendors = (a['vendors'][key].vendor_id);
                if (vendors == this.ven_key) {
                  this.orders.push(a as OrderInterface);
                }
              });
            }
            });
            this.dashboard.vendorOrderCount = this.orders.length;
            resolve(this.orders);
          });
      }
    });

  }

  getCategories() {
    return new Promise(resolve => {
      this.firebaseService.getCategories().snapshotChanges().subscribe(categories => {
        this.categories = [];

        categories.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.categories.push(a as CategoryInterface);
        });
        resolve(this.categories);
        console.log(this.categories);
      });
    });
  }

  getSliders() {
    return new Promise(resolve => {
      this.firebaseService.getSliders().snapshotChanges().subscribe(sliders => {
        this.sliders = [];

        sliders.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.sliders.push(a as SliderInterface);
        });
        console.log(this.sliders)
        resolve(this.sliders);

      });
    });
  }

  getZones() {
    return new Promise(resolve => {
      this.firebaseService.getCities().snapshotChanges().subscribe(zones => {
        this.zones = [];

        zones.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.zones.push(a as CityInterface);
        });
        resolve(this.zones);

      });
    });
  }

  getDistricts() {
    return new Promise(resolve => {
      this.firebaseService.getDistricts().snapshotChanges().subscribe(districts => {
        this.districts = [];

        districts.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.districts.push(a as CategoryInterface);
        });
        resolve(this.districts);

      });
    });
  }

  async initialize() {
    let vendor = await this.auth.checkVendor();
    if (vendor === true) {
      console.log("I am hrere");
      this.orders = await this.getCurrentUserOrders();
      this.products = await this.getProducts();
      this.dashboard.orderCount = this.orders.length;
      this.dashboard.productCount = this.products.length;
      localStorage.setItem('orderCount', this.orders.length)
      localStorage.setItem('productCount', this.products.length)
    }
  }
}
