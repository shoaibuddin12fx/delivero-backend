import { UserInterface } from './../models/user';
import { Injectable } from '@angular/core';

import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/database'; // Firebase modules for Database, Data list and Single object

import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  listings: AngularFireList<any[]>;
  listing: AngularFireObject<any>;

  restaurants: AngularFireList<any[]>;
  restaurant: AngularFireObject<any>;

  categories: AngularFireList<any[]>;
  category_details: AngularFireObject<any>;

  users: AngularFireList<any[]>;
  user_details: AngularFireObject<any>;

  sliders: AngularFireList<any[]>;
  slider_details: AngularFireObject<any>;

  vendor: AngularFireList<any[]>;
  vendor_details: AngularFireObject<any>;

  items: AngularFireList<any[]>;
  item_details: AngularFireObject<any>;

  orders: AngularFireList<any[]>;
  order_details: AngularFireObject<any>;

  allTableOrders: AngularFireList<any[]>;
  allTableOrdersDetails: AngularFireObject<any>;

  cities: AngularFireList<any[]>;
  cityDetail: AngularFireObject<any>;

  districts: AngularFireList<any[]>;
  districtDetail: AngularFireObject<any>;

  streets: AngularFireList<any[]>;
  streetDetail: AngularFireObject<any>;

  buildings: AngularFireList<any[]>;
  buildingDetail: AngularFireObject<any>;

  extraItemList: AngularFireList<any[]>;
  extraItemDetail: AngularFireObject<any>;

  chooseCategories: AngularFireList<any[]>;
  category_choose_details: AngularFireObject<any>;

  itemChoose: AngularFireList<any[]>;
  item_choose_details: AngularFireObject<any>;

  extraChooseItemList: AngularFireList<any[]>;
  extraChooseItemDetail: AngularFireObject<any>;

  restaurantOwnerInfo: AngularFireObject<any>;

  about: AngularFireList<any[]>;

  folder: any;
  itemFolder: any;
  restaurantFolder: any;
  categoryFolder: any;
  sliderFolder: any;
  vendorFolder: any;
  orderList: any;
  itemExtraList: any;
  itemChooseExtraList: any;
  userDetail: any;
  allTableOrdersList: any;
  // allTableOrders: any;
  paypal: any;
  stripe: any;
  tax: any;

  productId: any;

  constructor(private af: AngularFireDatabase) {
    this.listings = this.af.list('/listings') as AngularFireList<Listing[]>;
    this.restaurants = this.af.list('/restaurants') as AngularFireList<
      Listing[]
    >;
    this.categories = this.af.list('/category') as AngularFireList<Category[]>;
    this.sliders = this.af.list('/slider') as AngularFireList<Slider[]>;
    this.vendor = this.af.list('/vendor') as AngularFireList<Vendor[]>;
    this.items = this.af.list('/items') as AngularFireList<Item[]>;
    this.orders = this.af.list('/orders') as AngularFireList<Order[]>;
    this.orderList = firebase.database().ref('/orders');
    this.itemExtraList = firebase.database().ref('/items');
    this.userDetail = firebase.database().ref('/users');
    this.allTableOrders = this.af.list('/AllTableOrders') as AngularFireList<
      TableOrder[]
    >;
    this.allTableOrdersList = firebase.database().ref('/AllTableOrders');
    this.paypal = firebase.database().ref('/paypal');
    this.stripe = firebase.database().ref('/stripe');
    this.cities = this.af.list('/city') as AngularFireList<City[]>;
    this.districts = this.af.list('/districts') as AngularFireList<District[]>;
    this.streets = this.af.list('/streets') as AngularFireList<Street[]>;
    this.buildings = this.af.list('/apartments') as AngularFireList<Building[]>;
    this.about = this.af.list('/about') as AngularFireList<About[]>;

    this.chooseCategories = this.af.list('/categoryChoose') as AngularFireList<
      Category[]
    >;
    this.itemChoose = this.af.list('/itemChoose') as AngularFireList<Item[]>;
    this.itemChooseExtraList = firebase.database().ref('/itemChoose');

    this.tax = firebase.database().ref('/tax');

    this.folder = 'listingimages';
    this.itemFolder = 'itemimages';
    this.restaurantFolder = 'restaurantimages';
    this.categoryFolder = 'categoryimages';
    this.sliderFolder = 'sliderimages';
    this.vendorFolder = 'vendorimages';
  }

  getTimeStamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  addTaxConfiguration(tax) {
    console.log(tax);

    this.tax.set({
      percent: tax.percent,
    });
  }

  activeRestaurant(id, restaurant) {
    firebase.database().ref('/restaurants').child(restaurant.$key).set({
      address: restaurant.address,
      description: restaurant.description,
      firebase_url: restaurant.firebase_url,
      image: restaurant.image,
      info: restaurant.info,
      lat: restaurant.lat,
      long: restaurant.long,
      mark: restaurant.mark,
      option: restaurant.option,
      outlet: restaurant.outlet,
      phonenumber: restaurant.phonenumber,
      title: restaurant.title,
      user_id: restaurant.user_id,
    });

    firebase
      .database()
      .ref('/suspendedRestaurant')
      .child(restaurant.$key)
      .remove();

    console.log(id);

    console.log(restaurant);
  }

  suspendRestaurant(id, restaurant) {
    firebase.database().ref('/suspendedRestaurant').child(restaurant.$key).set({
      address: restaurant.address,
      description: restaurant.description,
      firebase_url: restaurant.firebase_url,
      image: restaurant.image,
      info: restaurant.info,
      lat: restaurant.lat,
      long: restaurant.long,
      mark: restaurant.mark,
      option: restaurant.option,
      outlet: restaurant.outlet,
      phonenumber: restaurant.phonenumber,
      title: restaurant.title,
      user_id: restaurant.user_id,
    });

    firebase.database().ref('/restaurants').child(restaurant.$key).remove();

    console.log(id);

    console.log(restaurant);
  }

  restaurantSuspendedUser: AngularFireList<any[]>;
  getSuspendedRestaurantUser(owner_id) {
    this.restaurantSuspendedUser = this.af.list(
      '/suspendedRestaurant/',
      (ref) => ref.orderByChild('user_id').equalTo(owner_id)
    ) as AngularFireList<Restaurant[]>;

    return this.restaurantSuspendedUser;
  }

  restaurantUser: AngularFireList<any>;
  getRestaurantUser(owner_id) {
    //const uid = this.authService.getUserId();

    this.restaurantUser = this.af.list('/restaurants/', (ref) =>
      ref.orderByChild('user_id').equalTo(owner_id)
    ) as AngularFireList<Restaurant>;
    // .subscribe(data => {
    //  console.log(data);
    //});
    return this.restaurantUser;
  }

  eachMonthOrder: AngularFireList<any[]>;
  getEachMonthReport(year, month) {
    this.eachMonthOrder = this.af.list(
      'reportsMonth/' + year + '/' + month
    ) as AngularFireList<MonthReport[]>;

    return this.eachMonthOrder;
  }

  dayOrder: AngularFireList<any[]>;
  getDayOrder(year, month, day) {
    this.dayOrder = this.af.list(
      'reportsDay/' + year + '/' + month + '/' + day
    ) as AngularFireList<MonthReport[]>;

    return this.dayOrder;
  }

  yearOrder: AngularFireList<any[]>;
  getYearOrder(year) {
    this.yearOrder = this.af.list('reports/' + year) as AngularFireList<
      MonthReport[]
    >;

    return this.yearOrder;
  }

  getRestaurantOwnerInfo(id) {
    this.restaurantOwnerInfo = this.af.object(
      '/users/' + id
    ) as AngularFireObject<OwnerID>;
    return this.restaurantOwnerInfo;
  }

  getUserDetail(uid) {
    this.user_details = this.af.object('/users/' + uid) as AngularFireObject<UserInterface>;
    return this.user_details;
  }

  getUserList() {
    return this.users = this.af.list('users') as AngularFireList<UserInterface[]>;
  }

  deleteUser(id) {
    return this.users.remove(id);
  }
  monthOrder: AngularFireList<any[]>;

  getMonthOrder(year, month) {
    this.monthOrder = this.af.list(
      'reportsMonth/' + year + '/' + month
    ) as AngularFireList<MonthReport[]>;

    return this.monthOrder;
  }

  updateChooseExtraItem(id, extraItem) {
    firebase
      .database()
      .ref('/items')
      .child(this.productId)
      .child('extraOptions')
      .child(id)
      .update({
        name: extraItem.name,
        selected: 'false',
        value: extraItem.value,
      });
  }

  getItemEditExtraDetail(id) {
    console.log(this.productId);

    this.extraItemDetail = this.af.object(
      '/items/' + this.productId + '/extraOptions/' + id
    ) as AngularFireObject<Extra>;
    return this.extraItemDetail;
  }

  getItemExtraDetail(id) {
    this.extraItemList = this.af.list(
      '/items/' + id + '/extraOptions/'
    ) as AngularFireList<Extra[]>;
    return this.extraItemList;
  }

  setProductId(id) {
    this.productId = id;
  }

  onExtraItemDelete(item_id, extra_id) {
    //private items = this.af.database.list('listings');
    //items.remove(category);
    this.itemExtraList
      .child(item_id)
      .child('extraOptions')
      .child(extra_id)
      .remove();
  }

  addExtraItem(id, extraItem) {
    console.log(id);
    console.log(extraItem);

    firebase.database().ref('/items').child(id).child('extraOptions').push({
      name: extraItem.name,
      selected: 'false',
      value: extraItem.price,
    });
  }

  addStripeConfiguration(stripe) {
    console.log(stripe);

    this.stripe.set({
      publishable: stripe.publishable,
      secret: stripe.secret,
    });
  }

  addPaypalConfiguration(paypal) {
    console.log(paypal);

    this.paypal.set({
      sandbox: paypal.sandbox,
      production: paypal.production,
    });
  }

  updateBuilding(id, building) {
    return this.buildings.update(id, building);
  }

  getBuildingDetails(id) {
    this.buildingDetail = this.af.object(
      '/apartments/' + id
    ) as AngularFireObject<Building>;
    return this.buildingDetail;
  }

  updateStreet(id, street) {
    return this.streets.update(id, street);
  }

  getStreetDetails(id) {
    this.streetDetail = this.af.object('/streets/' + id) as AngularFireObject<
      Street
    >;
    return this.streetDetail;
  }

  updateDistrict(id, district) {
    return this.districts.update(id, district);
  }

  getDistrictDetails(id) {
    this.districtDetail = this.af.object(
      '/districts/' + id
    ) as AngularFireObject<District>;
    return this.districtDetail;
  }

  updateCity(id, city) {
    return this.cities.update(id, city);
  }

  getCityDetails(id) {
    this.cityDetail = this.af.object('/city/' + id) as AngularFireObject<City>;
    return this.cityDetail;
  }

  addNewBuilding(buildingName) {
    return this.buildings.push(buildingName);
  }

  addNewStreet(streetName) {
    return this.streets.push(streetName);
  }

  addNewDistrict(districtName) {
    return this.districts.push(districtName);
  }

  addNewCity(cityName) {
    return this.cities.push(cityName);
  }

  deleteBuilding(buildingKey) {
    return this.buildings.remove(buildingKey);
  }

  getBuildings() {
    return this.buildings;
  }

  deleteStreet(streetKey) {
    return this.streets.remove(streetKey);
  }

  getStreets() {
    return this.streets;
  }

  getDistricts() {
    return this.districts;
  }

  deleteDistrict(districtKey) {
    return this.districts.remove(districtKey);
  }

  getCities() {
    return this.cities;
  }

  deleteCity(cityKey) {
    return this.cities.remove(cityKey);
  }

  addNewOrder(order) {
    return this.orders.push(order);
  }

  updateVendorOrderStatus(order_details) {
    firebase.database().ref('/orders').child(order_details.id).child('vendors').child(order_details.index).update({
      status: order_details.status,
    });
  }

  adminMarkOrderasComplete(order_details) {
    firebase.database().ref('/orders').child(order_details.id).update({
      status: order_details.status,
    })
  }

  updateProductStatus(productDetails) {
    firebase.database().ref('/items').child(productDetails.id).update({
      status: productDetails.status,
    });
  }

  updateCitytStatus(citytDetails) {
    firebase.database().ref('/city').child(citytDetails.id).update({
      status: citytDetails.status,
    });
  }

  updateDistrictStatus(districtDetails) {
    firebase.database().ref('/districts').child(districtDetails.id).update({
      status: districtDetails.status,
    });
  }

  updateVendorStatus(vendorDetails) {
    return new Promise(resolve => {
      firebase.database().ref('/vendor').child(vendorDetails.id).update({
        status: vendorDetails.status,
      });
      resolve(true);
    })
  }

  updateSliderStatus(sliderDetails) {
    firebase.database().ref('/slider').child(sliderDetails.id).update({
      status: sliderDetails.status,
    });
  }

  updateCategoryStatus(categoryDetails) {
    firebase.database().ref('/category').child(categoryDetails.id).update({
      status: categoryDetails.status,
    });
  }

  updateVendorUser(obj) {
    firebase.database().ref('/users').child(obj.id).update({
        ven_key:  obj.ven_key,
        ven_name: obj.ven_name,
        displayName: obj.displayName
    })
  }

  updateOrder(id, order) {
    return this.orders.update(id, order);
  }

  deleteOrder(id) {
    return this.orders.remove(id);
  }

  getOrderDetail(id) {
    return this.orderList.child(id);
  }

  getOrders() {
    return this.orders;
  }

  recentOrders: AngularFireList<any>;
  getRecentOrders() {
    this.recentOrders = this.af.list('/orders/', (ref) =>
      ref.orderByChild('createdAt').limitToLast(5)
    ) as AngularFireList<Item>;
    return this.recentOrders;
  }
  getUserCount() {
    return firebase.database().ref('/users').once('value').then((snapshot) => {
       return snapshot.numChildren();
    });
  }

  getUserToken(uid) {
    return firebase.database().ref('/users/'+uid+'/token').once('value').then( snap => {
      console.log(snap.val())
      return snap.val();
    });
  }

  getVendorCount() {
    return firebase.database().ref('/vendor').once('value').then((snapshot) => {
       return snapshot.numChildren();
    });
  }

  getProductCount() {
    return firebase.database().ref('/items').once('value').then((snapshot) => {
       return snapshot.numChildren();
    });
  }

  getOrderCount() {
    return firebase.database().ref('/orders').once('value').then((snapshot) => {
       return snapshot.numChildren();
    });
  }

  getCategoriesCount() {
    return firebase.database().ref('/category').once('value').then((snapshot) => {
       return snapshot.numChildren();
    });
  }

  addItem(item) {
    return new Promise (resolve => {
      let storageRefItem = firebase.storage().ref();
      for (let selectedItemFile of [
        (<HTMLInputElement>document.getElementById('image')).files[0],
      ]) {
        //let path = '/${this.folder}/${selectedFile.name}';
        let pathItem = `/${this.itemFolder}/${selectedItemFile.name}`;
        let iRefItem = storageRefItem.child(pathItem);
        iRefItem.put(selectedItemFile).then((snapshot) => {
          item.image = pathItem;

          let storageRef = firebase.storage().ref();
          let spaceRef = storageRef.child(item.image);

          console.log(item.image);
          storageRef
            .child(item.image)
            .getDownloadURL()
            .then((url) => {
              // Set image url
              console.log(url);

              item.image_firebase_url = url;

              console.log(item);
              this.items.push(item);
              resolve(true);
            })
            .catch((error) => {
              resolve(false);
              console.log(error);
            });
          });
      }
    })
  }

  updateItemWithImage(id, item) {
    return this.items.update(id, item);
  }

  updateItem(id, item) {
    return this.items.update(id, item);
  }

  deleteItem(id) {
    return this.items.remove(id);
  }

  getItemDetails(id) {
    this.item_details = this.af.object('/items/' + id) as AngularFireObject<
      Item
    >;
    return this.item_details;
  }

  getItems() {
    return this.items;
  }

  deleteCategory(id) {
    return this.categories.remove(id);
  }

  updateCategoryWithImage(id, category) {
    return this.categories.update(id, category);
  }

  updateCategory(id, category) {
    return this.categories.update(id, category);
  }

  getCategoryDetails(cat_id) {
    this.category_details = this.af.object(
      '/category/' + cat_id
    ) as AngularFireObject<Category>;
    return this.category_details;
  }

  addCategory(category) {
    return new Promise (resolve => {
      let storageRefItem = firebase.storage().ref();
      for (let selectedItemFile of [
        (<HTMLInputElement>document.getElementById('image')).files[0],
      ]) {
        let pathItem = `/${this.categoryFolder}/${selectedItemFile.name}`;
        let iRefItem = storageRefItem.child(pathItem);
        iRefItem.put(selectedItemFile).then((snapshot) => {
          category.image = pathItem;

          let storageRef = firebase.storage().ref();
          let spaceRef = storageRef.child(category.image);

          console.log(category.image);
          storageRef
            .child(category.image)
            .getDownloadURL()
            .then((url) => {
              console.log(url);

              category.firebase_url = url;
              this.categories.push(category);
              resolve(true);
            })
            .catch((error) => {
              resolve(false);
              console.log(error);
            });
        });
      }
    });
  }

  getCategories() {
    this.categories = this.af.list('/category') as AngularFireList<Category[]>;
    return this.categories;
  }

  addSlider(slider) {
    return new Promise (resolve => {
      let storageRefItem = firebase.storage().ref();
      for (let selectedItemFile of [
        (<HTMLInputElement>document.getElementById('image')).files[0],
      ]) {
        let pathItem = `/${this.sliderFolder}/${selectedItemFile.name}`;
        let iRefItem = storageRefItem.child(pathItem);
        iRefItem.put(selectedItemFile).then((snapshot) => {
          slider.image = pathItem;

          let storageRef = firebase.storage().ref();
          let spaceRef = storageRef.child(slider.image);

          console.log(slider.image);
          storageRef
            .child(slider.image)
            .getDownloadURL()
            .then((url) => {
              console.log(url);

              slider.firebase_url = url;
              this.sliders.push(slider);
              resolve(true);
            })
            .catch((error) => {
              resolve(false);
              console.log(error);
            });
        });
      }
    })

  }

  getSliders() {
    this.sliders = this.af.list('/slider') as AngularFireList<Slider[]>;
    return this.sliders;
  }

  getSliderDetails(slider_id) {
    this.slider_details = this.af.object(
      '/slider/' + slider_id
    ) as AngularFireObject<Slider>;
    return this.slider_details;
  }

  updateSliderWithImage(id, slider) {
    return this.sliders.update(id, slider);
  }

  updateSlider(id, slider) {
    return this.sliders.update(id, slider);
  }

  deleteSlider(id) {
    return this.sliders.remove(id);
  }

  updateRestaurant(id, restaurant) {
    return this.restaurants.update(id, restaurant);
  }

  updateRestaurantWithImage(id, restaurant) {
    return this.restaurants.update(id, restaurant);
  }

  getRestaurantDetails(id) {
    this.restaurant = this.af.object('/restaurants/' + id) as AngularFireObject<
      Restaurant
    >;
    return this.restaurant;
  }

  restaurantCategory: AngularFireList<any>;
  getRestaurantCategories(id) {
    // const uid = this.authService.getUserId();

    this.restaurantCategory = this.af.list('/category/', (ref) =>
      ref.orderByChild('res_name').equalTo(id)
    ) as AngularFireList<Category>;
    // .subscribe(data => {
    //  console.log(data);
    // });
    return this.restaurantCategory;
  }

  /**
      this.restaurantCategory = this.af.list('/category/',
              {
            query: {
              orderByChild: 'res_name',
              equalTo: id
            }
            }) as AngularFireList<Category>;
        return this.restaurantCategory;
      }

    */

  deleteRestaurant(id) {
    return this.restaurants.remove(id);
  }

  getRestaurants() {
    return this.restaurants;
  }

  addRestaurant(restaurant) {
    let storageRefItem = firebase.storage().ref();
    for (let selectedItemFile of [
      (<HTMLInputElement>document.getElementById('image')).files[0],
    ]) {
      //let path = '/${this.folder}/${selectedFile.name}';
      let pathItem = `/${this.restaurantFolder}/${selectedItemFile.name}`;
      let iRefItem = storageRefItem.child(pathItem);
      iRefItem.put(selectedItemFile).then((snapshot) => {
        restaurant.image = pathItem;

        let storageRef = firebase.storage().ref();
        let spaceRef = storageRef.child(restaurant.image);

        console.log(restaurant.image);
        storageRef
          .child(restaurant.image)
          .getDownloadURL()
          .then((url) => {
            // Set image url
            console.log(url);

            restaurant.firebase_url = url;

            return this.restaurants.push(restaurant);
          })
          .catch((error) => {
            console.log(error);
          });
      });
    }
  }

  addVendor(vendor) {
    return new Promise (resolve => {
      let storageRefItem = firebase.storage().ref();
      for (let selectedItemFile of [
        (<HTMLInputElement>document.getElementById('logo')).files[0],
      ]) {
        let pathItem = `/${this.vendorFolder}/${selectedItemFile.name}`;
        let iRefItem = storageRefItem.child(pathItem);
        iRefItem.put(selectedItemFile).then((snapshot) => {
          vendor.logo = pathItem;

          let storageRef = firebase.storage().ref();
          let spaceRef = storageRef.child(vendor.logo);

          console.log(vendor.logo);
          storageRef
            .child(vendor.logo)
            .getDownloadURL()
            .then((url) => {
              console.log(url);

              vendor.firebase_url = url;
              this.vendor.push(vendor);
              resolve(true);
            })
            .catch((error) => {
              resolve(false);
              console.log(error);
            });
        });
      }
    })
  }

  getvendor() {
    this.vendor = this.af.list('/vendor') as AngularFireList<Vendor[]>;
    return this.vendor;
  }

  getVendorDetails(ven_id) {
    this.vendor_details = this.af.object(
      `/vendor/` + ven_id
    ) as AngularFireObject<Vendor>;
    return this.vendor_details;
  }

  updateVendor(id, vendor) {
    return this.vendor.update(id, vendor);
  }

  deleteVendor(id) {
    return this.vendor.remove(id);
  }

  updateVendorWithImage(id, vendor) {
    return this.vendor.update(id, vendor);
  }

  addAbout(about) {
    return this.about.push(about);
  }

  getAbout() {
    this.about = this.af.list('/about') as AngularFireList<About[]>;
    return this.about;
  }

  updateAbout(about, id) {
    return this.about.update(about, id);
  }

  vendorItems: AngularFireList<any>;
  getVendorItems(ven_key) {
    this.vendorItems = this.af.list('/items/', (ref) =>
      ref.orderByChild('vendor').equalTo(ven_key)
    ) as AngularFireList<Item>;
    return this.vendorItems;
  }
}

interface OwnerID {
  $key?: string;
  address?: string;
  cardnumber?: string;
  displayName?: string;
  email?: string;
  europeResult?: string;
  facebook?: string;
  first?: string;
  language?: string;
  lastName?: string;
  lat?: string;
  lng?: string;
  nation?: string;
  ownerId?: string;
  phone?: string;
  status?: string;
}

interface MonthReport {
  $key?: string;
  fee?: string;
  restaurantId?: string;
  restaurantName?: string;
  restaurantOwnerId?: string;
  total?: string;
}

interface Listing {
  $key?: string;
  title?: string;
  type?: string;
  image?: string;
  city?: string;
  owner?: string;
  bedrooms?: string;
  path?: any;
}

interface Restaurant {
  $key?: string;
  address?: string;
  description?: string;
  image?: string;
  info?: string;
  lat?: string;
  long?: string;
  mark?: string;
  option?: string;
  outlet?: string;
  phonenumber?: string;
  title?: string;
  firebase_url?: string;
  user_id?: string;
}

interface Category {
  $key?: string;
  cat_id?: string;
  cat_name?: string;
  res_name?: string;
  image?: string;
  firebase_url?: string;
  parent_category_id?: string;
  status?: string;
}

interface Slider {
  $key?: string;
  slider_id?: string;
  slider_name?: string;
  status?: string;
  sort?: string;
  image?: string;
  created_at?: string;
  firebase_url?: string;
  type?: string;
}

interface City {
  $key?: string;
  name?: string;
}

interface District {
  $key?: string;
  name?: string;
}

interface Street {
  $key?: string;
  name?: string;
}

interface Building {
  $key?: string;
  name?: string;
}

interface Item {
  $key?: string;
  available?: string;
  category?: string;
  description?: string;
  image?: string;
  name?: string;
  price?: string;
  stock?: string;
  categories?: string;
  percent?: string;
  image_firebase_url?: string;
  lenght?: string;
  sub_category?: string;
  vendor?: string;
  feature?: string;
  new_arrival?: string;
  status?: string;
  sort?: string;
  size?: Array<string>;
  color?: Array<string>;
}

interface Extra {
  $key?: string;
  name: string;
  selected: string;
  value: string;
}

interface Order {
  $key?: string;
  address_id?: string;
  created?: string;
  item_qty?: string;
  order_date_time?: string;
  payment_id?: string;
  product_firebase?: string;
  product_id?: string;
  product_image?: string;
  product_price?: string;
  product_total_price?: string;
  restaurant_id?: string;
  restaurant_name?: string;
  status?: string;
  user_id?: string;
  user_name?: string;
  restaurant_owner_id?: string;
  checked?: string;
  name?: string;
  email?: string;
  phone?: string;
  city?: string;
  district?: string;
  subtotal?: string;
  shipping_fee?: string;
  total_amount?: string;
}

interface TableOrder {
  $key?: string;
  restaurant_id?: string;
  restaurant_address?: string;
  restaurant_description?: string;
  restaurant_backgroundImage?: string;
  restaurant_firebase_url?: string;
  restaurant_icon?: string;
  restaurant_iconText?: string;
  restaurant_images?: string;
  restaurant_info?: string;
  restaurant_lat?: string;
  restaurant_long?: string;
  restaurant_mark?: string;
  restaurant_market?: string;
  restaurant_option?: string;
  restaurant_outlet?: string;
  restaurant_phonenumber?: string;
  restaurant_show?: string;
  restaurant_subtitle?: string;
  restaurant_title?: string;
  date?: string;
  person?: string;
  time?: string;
  userId?: string;
  status?: string;
  timeStamp?: string;
  reverseOrder?: string;
}

interface Vendor {
  $key?: string;
  ven_id?: string;
  ven_name?: string;
  shopname?: string;
  logo?: string;
  latitude?: string;
  longitude?: string;
  address?: string;
  phone1?: string;
  phone2?: string;
  email?: string;
  status?: string;
  date?: string;
  user_id?: string;
}

interface About {
  $key?: string;
  abouttitle?: string;
  aboutdesc?: string;
  facebook?: string;
  whatsapp?: string;
  viber?: string;
  telegram?: string;
  instagram?: string;
  youtube?: string;
  phone1?: string;
  phone2?: string;
  address?: string;
  app_version?: string;
  email?: string;
}