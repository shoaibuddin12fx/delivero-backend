import { Injectable } from '@angular/core';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  public userCount: number = 0;
  public vendorCount: number = 0;
  public orderCount: number = 0;
  public productCount: number = 0;
  public categoriesCount: number = 0;
  public vendorProductCount: number = 0;
  public vendorOrderCount: number = 0;
  constructor(
    public firebaseService: FirebaseService
  ) { }

   getUsersCount() {
    return new Promise(async resolve => {
      this.userCount = await this.firebaseService.getUserCount();
      resolve(this.userCount);
    });

  }

  getVendorsCount() {
    return new Promise(async resolve => {
      this.vendorCount = await this.firebaseService.getVendorCount();
      resolve(this.vendorCount);
    });

  }

  getOrdersCount() {
    return new Promise(async resolve => {
        this.orderCount = await this.firebaseService.getOrderCount();
        resolve(this.orderCount);
    });

  }

  getProductsCount() {
    return new Promise(async resolve => {
        this.productCount = await this.firebaseService.getProductCount();
        resolve(this.productCount);
    });
  }

  getCategoriesCount() {
    return new Promise(async resolve => {
      this.categoriesCount = await this.firebaseService.getCategoriesCount();
      resolve(this.categoriesCount);
    });
  }
}
