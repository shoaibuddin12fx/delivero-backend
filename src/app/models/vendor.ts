export interface VendorInterface {
  $key?: string;
  ven_id?: string;
  ven_name?: string;
  shopname?: string;
  logo?: string;
  latitude?: string;
  longitude?: string;
  address?: string;
  phone1?: string;
  phone2?: string;
  email?: string;
  user_id?: string;
  status?: string;
  date?: string;
}
