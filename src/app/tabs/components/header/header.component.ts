import { Component, OnInit } from '@angular/core';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { OrderInterface } from 'src/app/models/order';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AuthService } from 'src/app/services/auth.service';
import { UtilityService } from 'src/app/services/utility.service';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-top-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  ven_key: any;
  orderList: any;
  isVendor: any;
  constructor(
    private pubSub: NgxPubSubService,
    private firebaseService: FirebaseService,
    public dashboardService: DashboardService,
    private auth: AuthService,
    public utility: UtilityService,
    ) { }

  async ngOnInit(): Promise<void> {
    this.isVendor = await this.auth.checkVendor();
    this.orderList = await this.getRecentOrders();
  }

  toggleSidebar(){
    this.pubSub.publishEvent('togglesidebar', {});
  }

  onSearch($event){
    let val = $event.target.value;
    this.pubSub.publishEvent('onsearch', val);
  }

  getRecentOrders() {
    return new Promise(async resolve => {
      let vendor = await this.auth.checkVendor();
      this.ven_key = await this.auth.getVendorKey();
      if (vendor === false) {
        this.firebaseService.getRecentOrders().snapshotChanges().subscribe((orders) => {
          let list = [];
          console.log(orders)
          orders.forEach(item => {
            let a = item.payload.toJSON();
            a['$key'] = item.key;
            console.log(a);

            list.push(a as OrderInterface);
          });
          resolve(list);
        });
      } else if (vendor === true) {
        this.firebaseService
          .getRecentOrders()
          .snapshotChanges()
          .subscribe((orders) => {
            let list = [];
            orders.forEach((item) => {
              const a = item.payload.toJSON();
              a['$key'] = item.key;
              if (a['vendors']) {
                Object.keys(a['vendors']).forEach((key) => {
                  let vendors = (a['vendors'][key].vendor_id);
                  if (vendors == this.ven_key) {
                    list.push(a as OrderInterface);
                  }
                });
              }
            });
            resolve(list)
          });

      }
    })
}

}
