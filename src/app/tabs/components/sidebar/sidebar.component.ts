import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  isVendor: any;
  
  items = [
    {
      link: 'dashboard',
      icon: 'la-dashboard',
      name: 'Home'
    },
    {
      link: 'orders',
      icon: 'la-shopping-cart',
      name: 'Orders'
    },
    {
      link: 'vendors',
      icon: 'la-building-o',
      name: 'Vendors'
    },
    {
      link: 'users',
      icon: 'la-user',
      name: 'Users'
    },
    {
      link: 'categories',
      icon: 'la-database',
      name: 'Categories'
    },
    {
      link: 'products',
      icon: 'la-dropbox',
      name: 'Products'
    },
    {
      link: 'zones',
      icon: 'la-map-marker',
      name: 'Zones'
    },
    {
      link: 'districts',
      icon: 'la-map-marker',
      name: 'Districts'
    },
    {
      link: 'sliders',
      icon: 'la-image',
      name: 'Sliders'
    },
    {
      link: 'notifications',
      icon: 'la-image',
      name: 'Notifications'
    },
  ]




  constructor(private authService: AuthService, public dashboardService: DashboardService, private data: DataService) { }

  async ngOnInit(): Promise<void> {
    this.isVendor = await this.authService.checkVendor();
  }

  onLogout() {
    this.authService.logoutUser();
  }

}
