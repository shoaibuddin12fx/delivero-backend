import { Component, OnInit } from '@angular/core';
import { RestaurantInterface } from 'src/app/models/restaurant';
import { CategoryInterface } from 'src/app/models/category';
import { ItemInterface } from 'src/app/models/item';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { VendorInterface } from 'src/app/models/vendor';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  Arr = Array;
  id: any;

  restaurant: any;
  imageUrl: any;
  categories: any;
  item: any;
  categoryList: any;
  vendorList: any;
  colors: Array<string> = [];
  sizes: Array<string> = [];
  
  private RestaurantInterface: RestaurantInterface[];
  private CategoryInterface: CategoryInterface[];
  private ItemInterface: ItemInterface[];

  public isVendor: any = null;
  public userUid: string = null;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private data: DataService
  ) {}

  async ngOnInit(): Promise<void> {
    this.isVendor = await this.authService.checkVendor();
    this.id = this.route.snapshot.params['id'];
    this.categoryList = await this.data.getCategories();
    this.vendorList = await this.data.getVendors();


    this.firebaseService
      .getItemDetails(this.id)
      .snapshotChanges()
      .subscribe((item) => {
        this.item = [];
        //  restaurant.forEach(item => {

        //  console.log(item);

        const res = item.payload.toJSON();
        res['$key'] = item.key;

        console.log(item);

        this.item = res as ItemInterface;
        // this.restaurant.push(res as RestaurantInterface);

        console.log(this.item);
        Object.keys(this.item.size).forEach((key) => {
          this.sizes.push(this.item.size[key]);
        });
        Object.keys(this.item.color).forEach((key) => {
          this.colors.push(this.item.color[key]);
        });
      });

  }

  getCategoryName($key) {
    if ($key && this.categoryList) {
      return this.categoryList.find((x) => x.$key === $key).cat_name;

    }
  }

  getvendorName($key) {
    if ($key && this.vendorList) {
      return this.vendorList.find((x) => x.$key === $key).ven_name;
    }
  }

}
