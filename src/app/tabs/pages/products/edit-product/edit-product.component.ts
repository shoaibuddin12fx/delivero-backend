import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { ItemInterface } from 'src/app/models/item';
import { CategoryInterface } from 'src/app/models/category';
import { VendorInterface } from 'src/app/models/vendor';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  id: any;
  name: any;
  description: any;
  available: any;
  price: any;
  stock: any;
  image: any;
  image_firebase_url: any;
  percent: any;
  saleprice: any;
  sub_category: any;
  vendor: any;
  status: any;
  sort: any;
  feature: any;
  new_arrival: any;
  categories: any;
  vendor_key: any;

  imageUrl: any;

  categoryList: any;
  sub_categoryList: any;
  vendorList: any;
  itemFolder: any;

  item: any;
  color: any;
  size: any;

  colors: Array<string> = [];
  sizes: Array<string> = [];

  isVendor: any;
  user: any;
  isImage = false;

  config = {
    displayKey: 'detail_string',
    search: true,
    height: '250px',
    placeholder: 'Vendor',
    limitTo: this.vendorList?.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search Vendor',
    searchOnKey: 'detail_string',
    clearOnSelection: false,
    inputDirection: 'ltr',
  };
  mainCategoryList: any;

  constructor(
    private data: DataService,
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private storage: AngularFireStorage
  ) {
    this.itemFolder = 'itemimages';
  }

  @Input() userUid: string;

  @ViewChild('imageUser') inputImageUser: ElementRef;

  uploadPercent: Observable<any>;
  urlImage: Observable<string>;

  async ngOnInit() {
    this.checkVendor();
    console.log('Here is Edit Item Page');
    this.mainCategoryList = await this.data.getCategories();
    this.categoryList = this.mainCategoryList.filter(x => x.parent_category_id === '');
    this.vendorList = await this.data.getVendors();

  
    this.id = this.route.snapshot.params['id'];

    this.firebaseService
      .getItemDetails(this.id)
      .snapshotChanges()
      .subscribe((item) => {
        this.item = [];
        //   restaurant.forEach(item => {

        //   console.log(item);

        let res = item.payload.toJSON();
        res['$key'] = item.key;

        console.log(res);

        this.item = res as ItemInterface;

        console.log(this.item);

        this.name = this.item.name;
        this.description = this.item.description;
        this.available = this.item.available;
        this.price = this.item.price;
        this.saleprice = this.item.saleprice;
        this.stock = this.item.stock;
        this.status = this.item.status;
        this.new_arrival = this.item.new_arrival;
        this.sub_category = this.item.sub_category;
        this.categories = this.item.categories;
        this.percent = this.item.percent;
        this.sort = this.item.sort;
        this.vendor_key = this.item.vendor;
        this.feature = this.item.feature;
        this.saleprice = this.item.saleprice;
        this.image_firebase_url = this.item.image_firebase_url;

        if (this.item.size) {
          Object.keys(this.item.size).forEach((key) => {
            this.sizes.push(this.item.size[key]);
          });
        }
        if (this.item.color) {
          Object.keys(this.item.color).forEach((key) => {
            this.colors.push(this.item.color[key]);
          });
        }

        this.sub_categoryList = this.mainCategoryList.filter(x => x.parent_category_id === this.categories);
        let sub_category_obj = this.sub_categoryList.find((y) => y.$key === this.item.sub_category);
        this.sub_category = sub_category_obj.cat_name;
        console.log(this.sub_category);
        console.log(this.id);

      });

  }

  onItemEditSubmit() {
    console.log(this.image);

    if (
      !this.inputImageUser.nativeElement.value ||
      this.inputImageUser.nativeElement.value == undefined
    ) {
      console.log('inside');

      if (this.user.roles.admin === true) {
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          sub_category: this.sub_category,
          categories: this.categories,
          saleprice: this.saleprice,
          vendor: this.vendor_key,
          percent: this.percent,
          sort: this.sort,
          feature: this.feature,
          new_arrival: this.new_arrival,
          image: this.image_firebase_url,
          image_firebase_url: this.image_firebase_url,
          size: this.sizes,
          color: this.colors,
        };

        this.firebaseService.updateItem(this.id, item);

        this.router.navigate(['/tabs/products']);

      } else if (this.user.roles.vendor === true) {
        this.vendor = this.user.ven_key;
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          sub_category: this.sub_category,
          categories: this.categories,
          saleprice: this.saleprice,
          vendor: this.vendor_key,
          percent: this.percent,
          sort: this.sort,
          feature: this.feature ? this.feature : '',
          new_arrival: this.new_arrival ? this.new_arrival : '',
          image: this.image_firebase_url,
          image_firebase_url: this.image_firebase_url,
          size: this.sizes,
          color: this.colors,
        };

        this.firebaseService.updateItem(this.id, item);

        this.router.navigate(['/tabs/products']);
      }

    }

    if (this.inputImageUser.nativeElement.value) {
      console.log('white');
      if (this.user.roles.admin === true) {
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          sub_category: this.sub_category,
          categories: this.categories,
          vendor: this.vendor_key,
          percent: this.percent,
          saleprice: this.saleprice,
          sort: this.sort,
          feature: this.feature,
          new_arrival: this.new_arrival,
          image: this.inputImageUser.nativeElement.value,
          image_firebase_url: this.inputImageUser.nativeElement.value,
          size: this.sizes,
          color: this.colors,
        };

        this.firebaseService.updateItemWithImage(this.id, item);

        this.router.navigate(['/tabs/products']);

      } else if (this.user.roles.vendor === true) {
        this.vendor = this.user.ven_key;
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          sub_category: this.sub_category,
          categories: this.categories,
          vendor: this.vendor_key,
          percent: this.percent,
          saleprice: this.saleprice,
          sort: this.sort,
          feature: this.feature ? this.feature : '',
          new_arrival: this.new_arrival ? this.new_arrival : '',
          image: this.inputImageUser.nativeElement.value,
          image_firebase_url: this.inputImageUser.nativeElement.value,
          size: this.sizes,
          color: this.colors,
        };

        this.firebaseService.updateItemWithImage(this.id, item);

        this.router.navigate(['/tabs/products']);
      }

    }
  }

  onChange($event) {
    // let file = $event.target.files[0]; //  <--- File Object for future use.
    console.log($event);
    this.image = $event; //  <--- File Object for future use.
  }

  onUpload(e) {
    this.isImage = true
    //  console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    // const filePath = `uploads/profile`;
    const filePath = `/${this.itemFolder}/${file.name}`;

    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();


    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();

    alert('Please wait for uploading images');

    console.log(ref.getDownloadURL());

    console.log(this.urlImage);
  }

  setParentCategory(cat_id) {
    console.log(cat_id);
    this.sub_categoryList = this.categoryList.filter(
      (x) => x.parent_category_id == cat_id
    );
    console.log(this.sub_categoryList)
  }

  addColor(color) {
    console.log(color);
    let index = this.colors.findIndex(x => x == color)
    console.log(index);

    if (index === -1) {
      this.colors.push(color);
      color = undefined;
      console.log(this.colors);
    } else if (index === 0) {
      return;
    }
  }
  removeColor(i) {
    this.colors.splice(i, 1);
  }

  addSize(size) {
    console.log(size);
    let index = this.sizes.findIndex(x => x == size)
    console.log(index);

    if (index === -1) {
      this.sizes.push(size);
      size = undefined;
      console.log(this.sizes);
    } else if (index === 0) {
      return;
    }
  }
  removeSize(i) {
    this.sizes.splice(i, 1);
  }

  checkVendor() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {
      this.isVendor = false;
    } else if (this.user.roles.vendor === true) {
      this.isVendor = true;
    }
  }

  selectionChanged($event) {
    if ($event.value) {
      console.log(this.vendor);
      this.vendor_key = $event.value.$key;
    }
  }
}
