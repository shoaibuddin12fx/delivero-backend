import { Component, OnInit, OnDestroy } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UserInterface } from 'src/app/models/user';
import { VendorInterface } from 'src/app/models/vendor';
import { ItemInterface } from 'src/app/models/item';
import { OrderInterface } from 'src/app/models/order';
import { UtilityService } from 'src/app/services/utility.service';
import { DataService } from 'src/app/services/data.service';
import { AuthService } from 'src/app/services/auth.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  usersLength: any;
  vendorsLength: any;
  productsLength: any;
  zones: any;
  districts: any;
  recentOrders: any = [];
  isLoading: any;
  ordersLength: any;
  ven_key: any

  subscription: Subscription;
  searchText = '';

  isVendor:any;

  order: string;
  reverse: any;

  constructor(
    public firebaseService: FirebaseService,
    public utility: UtilityService,
    public dataService: DataService,
    public auth: AuthService,
    public dashboard: DashboardService,
    private pubSub: NgxPubSubService
  ) { }


  ngOnInit(): void {
    this.auth.checkVendor();
    this.initialize();
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onSearch($event) {
    this.searchText = $event;
  }

  async initialize() {
    this.isLoading = true;
    this.utility.showLoader();
    this.isVendor = await this.auth.checkVendor();
    this.zones = await this.dataService.getZones();
    this.districts = await this.dataService.getDistricts();
    this.recentOrders = await this.getRecentOrders();
    // if (this.isVendor === false) {
    //   this.usersLength = await this.dashboard.getUsersCount();
    //   this.vendorsLength = await this.dashboard.getVendorsCount();
    //   this.productsLength = await this.dashboard.getProductsCount();
    //   this.ordersLength = await this.dashboard.getOrdersCount();
    // } else if (this.isVendor === true) {
    //   await this.dataService.initialize();
    //   this.productsLength = this.dashboard.productCount;
    //   this.ordersLength = this.dashboard.orderCount;
    // }
    this.utility.hideLoader();
    this.isLoading = false;
  }

   getRecentOrders() {
    return new Promise(async resolve => {
      let vendor = await this.auth.checkVendor();
      this.ven_key = await this.auth.getVendorKey();
      if (vendor === false) {
        this.firebaseService.getRecentOrders().snapshotChanges().subscribe((orders) => {
          let list = [];
          console.log(orders)
          orders.forEach(item => {
            let a = item.payload.toJSON();
            a['$key'] = item.key;
            console.log(a);

            list.push(a as OrderInterface);
          });
          console.log(list);
          resolve(list);
        });
      } else if (vendor === true) {
        this.firebaseService
          .getRecentOrders()
          .snapshotChanges()
          .subscribe((orders) => {
            let list = [];
            orders.forEach((item) => {
              const a = item.payload.toJSON();
              a['$key'] = item.key;
              if (a['vendors']) {
                Object.keys(a['vendors']).forEach((key) => {
                  let vendors = (a['vendors'][key].vendor_id);
                  if (vendors == this.ven_key) {
                    list.push(a as OrderInterface);
                  }
                });
              }
            });
            resolve(list)
          });

      }
    })
}

  getZoneName($key) {
    if ($key && this.zones) {
      return this.zones.find((x) => x.$key === $key).name;
    }
  }

  getSubZoneName($key) {
    if ($key && this.districts) {
      return this.districts.find((x) => x.$key === $key).name;
    }
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
}
