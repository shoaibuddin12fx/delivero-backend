import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit, OnDestroy {

  vendors: any;

  subscription: Subscription;
  searchText = '';

  vendorAdded: any;

  order: string = 'created_at';
  reverse: any = true;
  constructor(
    public dataService: DataService,
    private pubSub: NgxPubSubService,
    private firebaseService: FirebaseService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {
    this.vendors = await this.dataService.getVendors();
    console.log(this.vendors);
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
    this.vendorAdded = this.pubSub.subscribe('vendorAdded', async (data: any) => {
      if (data) {
        this.vendors = await this.dataService.getVendors();
      }
    });
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onSearch($event) {
    this.searchText = $event;
  }

  async deleteVendor($key) {
    let res = await this.utility.presentConfirm();
    if (res) {
      let index = this.vendors.findIndex(x => x.$key === $key);
      if (index !== -1) {
        console.log(this.vendors[index].$key);
        this.firebaseService.deleteVendor(this.vendors[index].$key);
        this.vendors.splice(index, 1);
      }
      this.utility.presentSuccess('Deleted');
    }
  }

  async updatevendorsStatus($key, status) {
    let flag = await this.utility.presentConfirmForStatus("Changing Vendor\'s Status Will Also Change It\'s Products\'s Status !!!");
    if (flag) {

      this.utility.showLoader();

      let obj = {
        id: $key,
        status: status
      };
      await this.firebaseService.updateVendorStatus(obj);
      this.firebaseService.getVendorItems($key).snapshotChanges().subscribe((items) => {
        // Calling All The Products For Vendor
        items.forEach(item => {
          let a = item.payload.toJSON();
          a["$key"] = item.key;
  
          let obj = {
            id: a["$key"],
            status: status
          }

          // Changing The Status Of Vendor's Products
          this.firebaseService.updateProductStatus(obj);
        });
        console.log("dsdasdasdasdasdasds");
        this.utility.hideLoader();
      })
    }
    console.log("DONENENENENENEN");
    this.vendors = await this.dataService.getVendors();
    this.utility.presentSuccess("Vendor\'s Status Updated");
  }
}
