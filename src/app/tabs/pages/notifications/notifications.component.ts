import { Component, OnInit } from '@angular/core';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notification: any;
  title: any;

  constructor(
    private network: NetworkService
  ) { }

  ngOnInit(): void {
  }

  sendNotification() {
    let obj = {
      to: '/topics/all',

      notification: {
        body: this.notification,
        title: this.title,
        color: '#000000',
      },
      data: {
        body: this.notification,
        title: this.title,
      }
    };

    let reqOptions = {
      headers: {
          'Content-Type': 'application/json',
          'Authorization' : 'key=AAAA6EiuSQE:APA91bHX5dMS3a9Gg6s0c260MByTCyTULo02cszqAyQtaEw8JoEqhHrYlRV9BES24DHyazDZrOE6Lvjlbb4vlLbvAn0LBkfXF1282FqeivNizTzvri6py6eJs708WSch0YlaZmObOLJF'
      }
    }
    this.network.sendPushNotification(obj, reqOptions);
  }
}
