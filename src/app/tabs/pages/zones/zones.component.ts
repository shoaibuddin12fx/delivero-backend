import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-zones',
  templateUrl: './zones.component.html',
  styleUrls: ['./zones.component.scss']
})
export class ZonesComponent implements OnInit, OnDestroy {

  zones: any;

  subscription: Subscription;
  searchText = '';

  order: string = 'created_at';
  reverse: any = true;
  currentDate = new Date();
  constructor(
    public dataService: DataService,
    private pubSub: NgxPubSubService,
    private firebaseService: FirebaseService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {
    this.zones = await this.dataService.getZones();
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onSearch($event){
    this.searchText = $event;
  }

  async deleteZone($key) {
    let res = this.utility.presentConfirm();
    if (res) {
      let index = this.zones.findIndex(x => x.$key === $key);
      if (index !== -1) {
        console.log(this.zones[index].$key);
        this.firebaseService.deleteCity(this.zones[index].$key);
        this.zones.splice(index, 1);
      }
      this.utility.presentSuccess('Deleted');
    }
  }

  async updateCityStatus($key, status) {
    let obj = {
      id: $key,
      status: status
    };
    this.firebaseService.updateCitytStatus(obj);
    this.zones = await this.dataService.getZones();
  }

}
