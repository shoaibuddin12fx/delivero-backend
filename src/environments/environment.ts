// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    // apiKey: "AIzaSyC7OssIThUAOUo-QcjejeQ1-ytjw-QOElA",
    // authDomain: "multi4ionic.firebaseapp.com",
    // databaseURL: "https://multi4ionic.firebaseio.com",
    // projectId: "multi4ionic",
    // storageBucket: "multi4ionic.appspot.com",
    // messagingSenderId: "719400134543"
    apiKey: "AIzaSyCJiayGAdigK5KbEjvPv6T9U6-aCM0BlPg",
    authDomain: "deliveryist.firebaseapp.com",
    databaseURL: "https://deliveryist.firebaseio.com",
    projectId: "deliveryist",
    storageBucket: "deliveryist.appspot.com",
    messagingSenderId: "107696559107",
    appId: "1:107696559107:web:bca5054ecb8ad95f946a36",
    measurementId: "G-LRX0XJZW3G"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
